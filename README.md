# 欢迎光临

虽然这是一个测试仓库，但是也欢迎大家在这里交流！

也欢迎大家试用我们的代码托管平台的各种功能，在[问题列表](https://code.opensource.huaweicloud.com/example/example/issues)里给我们提建议！

# 追踪改进

在问题列表里，已经有很多朋友提了各种建议，我们将值得参考的先列出来，然后追踪改进。

[#6 希望能在代码浏览页面默认显示当前目录的README](https://code.opensource.huaweicloud.com/example/example/issues/6)   进行中...
[#7 你们搞得这个git啊，exciting！](https://code.opensource.huaweicloud.com/example/example/issues/7)  进行中...
[#8 风格好乱的感觉](https://code.opensource.huaweicloud.com/example/example/issues/8)    新建问题已经上线
[#9 功能问题和UI问题](https://code.opensource.huaweicloud.com/example/example/issues/9)  进行中...
[#36 commit 信息不规范,而且信息太随意](https://code.opensource.huaweicloud.com/example/example/issues/36)  Commit信息，已经修改
[#39 页面底部没有版权信息，万一技术和页面一起被美帝偷了怎么办](https://code.opensource.huaweicloud.com/example/example/issues/39)  进行中...

[#70](https://code.opensource.huaweicloud.com/example/example/issues/70) [#71](https://code.opensource.huaweicloud.com/example/example/issues/71) [#76](https://code.opensource.huaweicloud.com/example/example/issues/76) [#77](https://code.opensource.huaweicloud.com/example/example/issues/77) [#78](https://code.opensource.huaweicloud.com/example/example/issues/78)  均为访问频率控制导致的问题，已经修复
[#105 咋修改跟人头像和基本信息,没看见入口啊?](https://code.opensource.huaweicloud.com/example/example/issues/105)  进行中...

# 谢谢大家